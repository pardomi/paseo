package ca.chancehorizon.paseo


import android.os.Bundle
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.view.ContextThemeWrapper
import androidx.fragment.app.Fragment
import ca.chancehorizon.paseo.databinding.EditStepsBottomsheetBinding


class EditStepsEntryFragment : BottomSheetDialogFragment(), View.OnClickListener {

    // Scoped to the lifecycle of the fragment's view (between onCreateView and onDestroyView)
    private var editStepsEntryFragmentBinding: EditStepsBottomsheetBinding? = null

    lateinit var paseoDBHelper : PaseoDBHelper

    var displayDate: String = " - "
        get() = field        // getter
        set(value) {         // setter
            field = value
        }

    var date: Int = 20001225
        get() = field        // getter
        set(value) {         // setter
            field = value
        }

    var hour: Int = 12
        get() = field        // getter
        set(value) {         // setter
            field = value
        }

    var recordedSteps: Int = 0
        get() = field        // getter
        set(value) {         // setter
            field = value
        }

    var editedSteps: Int = 0
        get() = field        // getter
        set(value) {         // setter
            field = value
        }

    var parent: Fragment? = null
        get() = field        // getter
        set(value) {         // setter
            field = value
        }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        // point to the Paseo database that stores all the daily steps data
        paseoDBHelper = PaseoDBHelper(requireActivity())

        val theTheme = requireContext().getTheme()
        val contextThemeWrapper = ContextThemeWrapper(activity, theTheme)

        val view = inflater.cloneInContext(contextThemeWrapper).inflate(R.layout.edit_steps_bottomsheet, container, false)

        val binding = EditStepsBottomsheetBinding.bind(view)
        editStepsEntryFragmentBinding = binding

        return view
    }



    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // respond to the user tapping anywhere in the dialog
        editStepsEntryFragmentBinding!!.editStepsLayout.setOnClickListener(this)

        editStepsEntryFragmentBinding!!.saveButton.setOnClickListener(this)
        editStepsEntryFragmentBinding!!.cancelButton.setOnClickListener(this)
        editStepsEntryFragmentBinding!!.revertButton.setOnClickListener(this)


        // fill in the details for this timeUnit
        updateDetail()
    }



    override fun onDestroy() {
        // Do not store the binding instance in a field, if not required.
        editStepsEntryFragmentBinding = null

        super.onDestroy()
    }



    fun updateDetail() {

        editStepsEntryFragmentBinding!!.date.setText(displayDate)
        editStepsEntryFragmentBinding!!.editStepsRecordedValue.setText(getString(R.string.recorded_steps_num, recordedSteps))
        editStepsEntryFragmentBinding!!.editedStepsEntry.setText(editedSteps.toString())
    }



    // respond to user taps
    override fun onClick(view: View) {

        when (view) {
            editStepsEntryFragmentBinding!!.saveButton -> {

                // save edited steps to database
                val newEditSteps = editStepsEntryFragmentBinding!!.editedStepsEntry.text.toString().toInt()

                paseoDBHelper.insertEditedSteps(StepsModel(0, date = date, hour = hour,
                        startSteps = 0, endSteps = newEditSteps - recordedSteps))
            }
            editStepsEntryFragmentBinding!!.cancelButton -> {
                dismiss()
            }
            editStepsEntryFragmentBinding!!.revertButton -> {
                // clear the edited steps from the table
                // delete the edited steps record from the database (if exists)
                editStepsEntryFragmentBinding!!.editedStepsEntry.text.toString().toInt()

                paseoDBHelper.insertEditedSteps(StepsModel(0, date = date, hour = hour,
                        startSteps = 0, endSteps = 0))
            }

        }

        // update the steps table in the Edit Steps screen
        (parentFragment as EditStepsFragment).createStepsTable()

        // close the dialog
        dismiss()

    }
}